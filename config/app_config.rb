# frozen_string_literal: true

require 'yaml'

# class AppConfig
class AppConfig
  def self.[](key)
    ENV[key] || conf[key]
  end

  def self.[]=(key, value)
    conf[key] = value
  end

  def self.conf_file
    @conf_file ||= File.dirname(__FILE__) + '/config.yml'
  end

  def self.conf
    @conf ||= YAML.load_file(conf_file)
  rescue StandardError => e
    warn e
    warn 'config.yml load error!!'
    exit 1
  end
end
