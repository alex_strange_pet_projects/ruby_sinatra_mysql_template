require 'rubygems'
require 'bundler/setup'
require 'rack'
require 'sinatra'
$stdout.sync = true if development?
require 'sinatra/reloader' if development?
require 'sinatra/content_for'
require 'json'

require File.dirname(__FILE__) + '/config/app_config'

folders = %w[config models controllers api api\/v1]

folders.flatten.uniq.each do |cat|
  Dir.glob("#{File.dirname(__FILE__)}/#{cat}/*.rb").each do |rb|
    puts "load #{rb}"
    require rb
  end
end

DataMapper::Logger.new($stdout, :debug)
DataMapper.finalize

run Sinatra::Application
